class SecretsController < ApplicationController

  def create
    params[:secret][:author_id] = current_user.id
    @secret = Secret.new(params[:secret])

    if @secret.save
      head :ok
    else
      render :json => @secret.errors.full_messages
    end
  end


  def new
    @users = User.where("id != ?", current_user.id)
  end
end
