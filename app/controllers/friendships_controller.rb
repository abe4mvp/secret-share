class FriendshipsController < ApplicationController

  def create
    @friendship = Friendship.new(in_friend_id: current_user.id, out_friend_id: params[:user_id])
    @friendship.save!
    head :ok
    #redirect_to users_url
  end

  def destroy
    @friendship = Friendship.find_by_in_friend_id_and_out_friend_id(
                                                    current_user.id,
                                                    params[:user_id])


    @friendship.destroy
    head :ok
    #redirect_to users_url
  end


end
