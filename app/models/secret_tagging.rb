class SecretTagging < ActiveRecord::Base
  attr_accessible :tag_id, :secret_id

  belongs_to(:secret)
  belongs_to(:tag)

end
