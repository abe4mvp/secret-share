class Friendship < ActiveRecord::Base
  attr_accessible :in_friend_id, :out_friend_id
  validate :no_self_friendship
  validate :no_duplicates
  validates :in_friend_id, :out_friend_id, presence: true

  belongs_to(
  :out_friend,
  class_name: "User",
  foreign_key: :out_friend_id,
  primary_key: :id
  )

  belongs_to(
  :in_friend,
  class_name: "User",
  foreign_key: :in_friend_id,
  primary_key: :id
  )

  def self.can_friend?(out_friend_id, in_friend_id)
    !Friendship.exists?(out_friend_id: out_friend_id, in_friend_id: in_friend_id) &&
    in_friend_id != out_friend_id
  end

  private
    def no_self_friendship
      if self.in_friend_id == self.out_friend_id
        errors[:friendship] << "can't friend yourself"
      end
    end

    def no_duplicates
      unless Friendship.can_friend?(self.out_friend_id, self.in_friend_id)
        errors[:friendship] << "can't duplicate friendship"
      end
    end

end
